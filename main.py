#encoding: utf-8
from __future__ import unicode_literals

import logging
import os
import glob
import telebot
import yt_dlp

from mutagen.mp3 import MP3
from mutagen import MutagenError
from config import TG_TOKEN
from config import FOLDER


#Constants
bot = telebot.TeleBot(TG_TOKEN)
LIMIT = 2300


#Logger for telebot
logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)



#Logger for youtube-dl
class MyLogger():
	'''Logger for youtube-dl'''
	def debug(self, msg):
		'''Debug messages from youtube-dl'''
	def warning(self, msg):
		'''Warnings from youtube-dl'''
	def error(self, msg):
		'''Errors from youtube-dl'''
		print(self, msg)



#Youtube-dl options
ydl_opts = {
	'updatetime': False,
	'outtmpl': FOLDER+'%(title)s.%(etx)s',
	'noplaylist': True,
	'format': 'bestaudio/best',
	'logger': MyLogger(),
	'postprocessors': [
	 						{'key': 'FFmpegExtractAudio',
							'preferredcodec': 'mp3',
							'preferredquality': '192'}
					  ]
	}



def video_checker(url, stream, length):
	'''Check video for sending'''

	if url is None or 'google' not in url:
		msg = 'Only from youtube.com'
		return msg
	if stream:
		msg = "Don't download streams"
		return msg
	if length > LIMIT:
		msg = 'File is too large'
		return msg

	return True



def video_downloader(link, message):
	'''Download, convert video to mp3 then send'''

	with yt_dlp.YoutubeDL(ydl_opts) as ydl:
		info = ydl.extract_info(link, download=False)
		stream = info.get('is_live', None)
		url = info.get('url', None)
		length = info.get('duration', None)


		video_avalible = video_checker(url, stream, length)

		if video_avalible is True:
			try:
				ydl.download([link])
				file_list = glob.glob(os.path.join(FOLDER, '*.mp3'))
				# Compare video duration
				for file_name in file_list:
					audio = MP3(file_name)
					duration = audio.info.length
					if int(round(duration))+1 >= length:

						with open (file_name, 'rb') as file:

							bot.send_chat_action(message.from_user.id, 'upload_audio')
							bot.send_audio(message.from_user.id, file, timeout=500, title=file_name.replace(FOLDER, ''))

						os.remove(str(file_name))

			except PermissionError:
				pass

		else:
			bot.send_message(message.from_user.id, video_avalible)



@bot.message_handler(content_types=['text'])
def handle_text(message):
	'''Handler for user's messages'''

	if message.text.lower() == '/start':
		bot.send_message(message.from_user.id, 'Welcome! Send me the video link')
	else:
		bot.send_message(message.from_user.id, 'Downloading...')

	try:
		video_downloader(message.text, message)

	# If link isn't valid
	except yt_dlp.utils.DownloadError:
		bot.send_message(message.from_user.id, 'The link is invalid (or blocked)')
	# File size must be 50 MB
	except telebot.apihelper.ApiException:
		bot.send_message(message.from_user.id, 'File is too large')
	# If link isn't valid
	except KeyError:
		bot.send_message(message.from_user.id, 'The link is invalid (or blocked)')
	except MutagenError:
		pass


#Infinite loop
bot.polling(none_stop=True, timeout=500)
